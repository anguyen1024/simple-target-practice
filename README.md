# Simple Target Practice
A simple app to improve your mouse accuracy

## Available Scripts
In the project directory, you can run:
```bash
npm start
npm test
npm run build
npm run eject
```