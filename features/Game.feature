Feature: Simple Target Practice app
  An application to help with mouse accuracy.

  Scenario: Targets spawn
    When the target practice starts
    Then a number of targets will be drawn in the screen

  Scenario: Target moves when clicked
    Given the target practice starts
    When a user performs a mouse down on the target
    Then the target will move to a new location after a brief delay

  Scenario: Target statistics are collected
    Given the target practice starts
    When a user performs a mouse down on the target
    Then 

