Feature: Player Statistics
  Actions performed in the game screen will be recorded and displayed

  Background:
    Given the target practice starts

  Scenario: Target hit statistic is collected
    This metric is used to indicate accuracy (positive)
    When a user performs a mouse down on the target
    And target hit count will increment

  Scenario: Target miss statistic is collected
    This metric is used to indicate accuracy (negative)
    When a user performs a mouse down on empty space
    Then target miss count will increment

  Scenario: Actions per minute statistic is collected
    This metric is used to indicate speed
    When a user performs an action in the game screen
    Then the APM will be recalculated

