import React, { Component } from "react";
import * as PIXI from "pixi.js";
import target from "./assets/target.png";

class App extends Component {
  constructor(props) {
    super(props);
    document.body.style.cursor = "crosshair";
    this.pixiContainer = null;
    this.app = new PIXI.Application({
      width: 800,
      height: 600,
      transparent: false
    });
    this.app.renderer.backgroundColor = 0xbdeeef;
    this.app.renderer.interactive = true;
  }

  render() {
    return (
      <div>
        <h1>Simple Target Practice</h1>
        <div ref={this.updatePixiContainer} />
      </div>
    );
  }

  updatePixiContainer = element => {
    // the element is the DOM object that we will use as container to add pixi stage(canvas)
    this.pixiContainer = element;
    // add the application to the DOM element from the ref
    if (this.pixiContainer && this.pixiContainer.children.length <= 0) {
      this.pixiContainer.appendChild(this.app.view);
      this.setup();
    }
  };

  setup() {
    PIXI.loader.add("target", target).load(this.initialize);
  }

  initialize = () => {
    // for (let i = 0; i < 10; i++) {
    //   this.app.stage.addChild(this.createTargetSprite());
    // }
    let count = 1;
    this.app.stage.addChild(this.createTargetSprite());
    const interval = setInterval(() => {
      if (count < 10) {
        this.app.stage.addChild(this.createTargetSprite());
        count++;
      } else clearInterval(this);
    }, 500);
  };

  createTargetSprite() {
    const targetSize = 30;
    const target = new PIXI.Sprite(PIXI.loader.resources["target"].texture);
    target.x = this.getRandomWidth(targetSize);
    target.y = this.getRandomHeight(targetSize);
    target.interactive = true;
    target.on("mousedown", event => {
      target.x = 0 - targetSize;
      target.y = 0 - targetSize;

      setTimeout(() => {
        target.x = this.getRandomWidth(targetSize);
        target.y = this.getRandomHeight(targetSize);
      }, 1000);
    });
    return target;
  }

  getRandomWidth = objectSize => {
    return Math.random() * (this.app.renderer.width - objectSize);
  };

  getRandomHeight = objectSize => {
    return Math.random() * (this.app.renderer.height - objectSize);
  };
}

export default App;
